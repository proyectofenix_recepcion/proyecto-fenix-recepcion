/**drop table Entretenimiento;
drop table Usuario;
drop table Membresia;
drop table Rol;
drop table Contenedor;
drop table Tipo_Entretenimiento;*/

CREATE TABLE Contenedor
  (
    idContenedor INTEGER NOT NULL ,
    nombre       VARCHAR2 (255)
  ) ;
  
ALTER TABLE Contenedor ADD CONSTRAINT Contenedor_PK PRIMARY KEY ( idContenedor ) ;

CREATE TABLE Entretenimiento
  (
    idEntretenimiento INTEGER NOT NULL ,
    idContenedor      INTEGER NOT NULL ,
    url               VARCHAR2 (255)
  ) ;
ALTER TABLE Entretenimiento ADD CONSTRAINT Entretenimiento_PK PRIMARY KEY ( idEntretenimiento ) ;

CREATE TABLE Membresia
  (
    idMembresia INTEGER NOT NULL ,
    nombre      VARCHAR2 (255) NOT NULL
  ) ;
ALTER TABLE Membresia ADD CONSTRAINT Membresia_PK PRIMARY KEY ( idMembresia ) ;

CREATE TABLE Rol
  ( idRol INTEGER NOT NULL , nombre VARCHAR2 (255)
  ) ;
ALTER TABLE Rol ADD CONSTRAINT Tipo_PK PRIMARY KEY ( idRol ) ;

CREATE TABLE Tipo_Entretenimiento
  (
    idTipo_Entretenimiento INTEGER NOT NULL ,
    nombre                 VARCHAR2 (255) ,
    idEntretenimiento      INTEGER NOT NULL
  ) ;
ALTER TABLE Tipo_Entretenimiento ADD CONSTRAINT Tipo_Entretenimiento_PK PRIMARY KEY ( idTipo_Entretenimiento ) ;

CREATE TABLE Usuario
  (
    idUsuario   INTEGER NOT NULL ,
    nombre      VARCHAR2 (255) ,
    nick        VARCHAR2 (255) ,
    contrasena  VARCHAR2 (255) ,
    correo      VARCHAR2 (255) ,
    idRol       INTEGER NOT NULL ,
    idMembresia INTEGER NOT NULL
  ) ;
ALTER TABLE Usuario ADD CONSTRAINT Usuario_PK PRIMARY KEY ( idUsuario ) ;


ALTER TABLE Entretenimiento ADD CONSTRAINT Entretenimiento_Contenedor_FK FOREIGN KEY ( idContenedor ) REFERENCES Contenedor ( idContenedor ) ;

--  ERROR: FK name length exceeds maximum allowed length(30)
ALTER TABLE Tipo_Entretenimiento ADD CONSTRAINT Tipo_Entrete_FK FOREIGN KEY ( idEntretenimiento ) REFERENCES Entretenimiento ( idEntretenimiento ) ;

ALTER TABLE Usuario ADD CONSTRAINT Usuario_Membresia_FK FOREIGN KEY ( idMembresia ) REFERENCES Membresia ( idMembresia ) ;

ALTER TABLE Usuario ADD CONSTRAINT Usuario_Rol_FK FOREIGN KEY ( idRol ) REFERENCES Rol ( idRol ) ;

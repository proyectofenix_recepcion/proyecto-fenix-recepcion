package org.pmc.bean;

public class Membresia {
	private Integer idMembresia;
	private String nombre;
	
	
	public Membresia(Integer idMembresia, String nombre) {
		super();
		this.idMembresia = idMembresia;
		this.nombre = nombre;
	}
	
	public Membresia(){
		super();
	}
	
	public Integer getIdMembresia() {
		return idMembresia;
	}
	public void setIdMembresia(Integer idMembresia) {
		this.idMembresia = idMembresia;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}

package org.pmc.bean;

public class Usuario {
	private Integer idUsuario;
	private String nombre;
	private String nick;
	private String contrasena;
	private String correo;
	private Rol rol;
	private Membresia membresia;
	
	
	
	public Usuario(Integer idUsuario, String nombre, String nick,
			String contrasena, String correo, Rol rol, Membresia membresia) {
		super();
		this.idUsuario = idUsuario;
		this.nombre = nombre;
		this.nick = nick;
		this.contrasena = contrasena;
		this.correo = correo;
		this.rol = rol;
		this.membresia=membresia;
	}
	
	public void setMembresia(Membresia member){
		membresia = member;
	}
	
	public Membresia getMembresia(){
		return membresia;
	}
	
	public Usuario() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public Rol getRol() {
		return rol;
	}
	public void setType(Rol type) {
		this.rol = type;
	}
	
	
	
}
